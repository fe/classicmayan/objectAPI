# [1.1.0](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/compare/v1.0.7...v1.1.0) (2022-12-05)


### Bug Fixes

* add gitlab ci yaml ([b24113a](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/b24113a867567eb0b7a977aab4f199cf38eda443))
* add m2 repo config file ([626c438](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/626c43838eca37d5e67c521ff59fe2eb5891a189))
* add missing files ([affec4e](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/affec4eebbc46620806a6d4ffe46ade5fbd6cd28))
* add more ci workflow files, add deb and sbom profiles ([b99a8b4](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/b99a8b4d4a20cfdacc101f15932ae6928dacb54d))
* increase some dependency versions ([47bfd0f](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/47bfd0f983a52f4f2ab3348a01e4885f3c942bd1))


### Features

* add new GitLab CI workflow, some code formatting ([39c8949](https://gitlab.gwdg.de/fe/classicmayan/objectAPI/commit/39c89497a9bce4a2cd8c998e6b596acb75ca213d))
