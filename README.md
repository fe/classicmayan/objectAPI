# ClassicMayan Tools – Object API

## General

The Object API delivers:

1. The data from ConedaKOR for usage in the RDF input mask (Artefact → Image in Maya Image Database). This is productive use.

2. Can be used for the sign catalogue to request metadata fields for a certain URI. They can be set as API parameters. Not implemented for every object. Nice to have and not in productive use.

## Releasing a new version

For releasing a new version of Object API, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).
