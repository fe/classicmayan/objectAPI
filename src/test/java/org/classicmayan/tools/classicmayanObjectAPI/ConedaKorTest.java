package org.classicmayan.tools.classicmayanObjectAPI;

import java.io.IOException;

import org.classicmayan.tools.signCatalogueAPI.ConedaKor;
import org.json.JSONException;
import org.junit.Test;

/**
 * 
 */
public class ConedaKorTest {

  /**
   * @throws JSONException
   * @throws IOException
   */
  @Test
  public void conedaKorGetArtefactIDs() throws JSONException, IOException {
    ConedaKor conedaKor = new ConedaKor();
    System.out.println(conedaKor.getArtefactIDs("tikal%20altar", "1"));
  }

}
