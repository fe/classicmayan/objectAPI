package org.classicmayan.tools.classicmayanObjectAPI;

import java.util.ArrayList;
import java.util.List;

import org.classicmayan.tools.signCatalogueAPI.Sign;
import org.junit.Ignore;
import org.junit.Test;

import junit.framework.TestCase;

/**
 * TODO Ignoring sign tests, not working, not productive --> please check!
 */
@Ignore
public class SignTest extends TestCase {

  /**
   * 
   */
  @Test
  public static void testBuildQuery() {

    List<String> fields = new ArrayList<String>();

    fields.add("status");
    fields.add("signNumber");
    fields.add("logographic_reading");
    fields.add("logographic_meaning");
    fields.add("syllabic_reading");
    fields.add("diacritic_function");
    fields.add("numeric_function");
    fields.add("distinct_allograph_to");
    fields.add("is_same_as_sign");
    fields.add("source");

    Sign sign = new Sign();
    String query = sign.buildQuery(fields, sign.getSignQueries());
    System.out.println(query);

    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:3r85m")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:2skzc")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:36jdh")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:3rcgn")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:30785")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:2skhj")), fields));
    System.out.println(sign.queryExecutionList(
        Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:3rx42")), fields));
    System.out
        .println(sign
            .queryExecutionList(
                Sign.executeQuery(query.replace("URI_TO_REPLACE", "textgrid:2stwt")), fields)
            .toString(2));
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testSignNumber() {
    Sign sign = new Sign();
    System.out.println(sign.getSignNumber("textgrid:3rwm7"));
  }

}
