package org.classicmayan.tools.signCatalogueAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class ConedaKor {

  /**
   * @param rd
   * @return
   * @throws IOException
   */
  private static String readAll(Reader rd) throws IOException {

    StringBuilder sb = new StringBuilder();
    int cp;
    System.out.println("\n");
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  /**
   * @param url
   * @return
   * @throws IOException
   * @throws JSONException
   */
  public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {

    InputStream is =
        new URL(java.net.URLEncoder.encode(url, "UTF-8").replace(" ", "+")).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONArray json = new JSONArray(jsonText);

      return json;

    } finally {
      is.close();
    }
  }

  /**
   * @param url
   * @return
   * @throws IOException
   * @throws JSONException
   */
  public static JSONObject readJsonObjectFromUrl(String url) throws IOException, JSONException {

    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);

      return json;

    } finally {
      is.close();
    }
  }

  /**
   * @param keyword
   * @param pageNumber
   * @return
   * @throws JSONException
   * @throws IOException
   */
  @GET
  @Path("/mediaForArtefact/term={keyword}/page={pageNumber}")
  @Produces(MediaType.APPLICATION_JSON)
  public String getArtefactIDs(@PathParam("keyword") String keyword,
      @PathParam("pageNumber") String pageNumber) throws JSONException, IOException {

    String url = "https://classicmayan.kor.de.dariah.eu/entities.json?terms="
        + keyword.replace(" ", "+") + "&kind_id=3&per_page=20&page=" + pageNumber;
    // System.out.println(url);
    // JSONArray resultsFromZoteroSearch = readJsonObjectFromUrl(url).getJSONArray("ids");
    JSONObject result = readJsonObjectFromUrl(url);
    List<String> ids = new ArrayList<String>();
    JSONArray idsAndNamesOfArtefacts = new JSONArray();
    // System.out.println(result);
    JSONArray resultingIDs = result.getJSONArray("records");

    for (int i = 0; i < resultingIDs.length(); i++) {
      JSONObject json = resultingIDs.getJSONObject(i);
      ids.add(json.get("id").toString());
      JSONObject jsonTmp = new JSONObject();
      jsonTmp.put("id", json.get("id").toString());
      jsonTmp.put("display_name", json.get("display_name").toString());
      idsAndNamesOfArtefacts.put(jsonTmp);

    }

    // System.out.println("CONTENT");
    // System.out.println(idsAndNamesOfArtefacts);
    JSONArray artefactIDsWithLinksToMediaArray = new JSONArray();
    // System.out.println(ids.size());

    for (int i = 0; i < ids.size(); i++) {
      JSONObject singleResult = new JSONObject();
      JSONObject mediaUrl = new JSONObject();
      // System.out.println(ids.get(i));
      mediaUrl = readJsonObjectFromUrl(
          "https://classicmayan.kor.de.dariah.eu/relationships.json?from_entity_id=" + ids.get(i)
              + "&relation_name=artefact%20is%20depicted%20by%20medium");

      // System.out.println("MEDIAURL: ");
      // System.out.println(mediaUrl);
      // System.out.println("https://classicmayan.kor.de.dariah.eu/relationships.json?entity_id=" +
      // ids.get(i) + "&relation_name=artefact%20is%20depicted%20by%20medium");

      JSONArray medium = mediaUrl.getJSONArray("records");
      // System.out.println(medium);
      JSONArray mediaArray = new JSONArray();
      // System.out.println("mediumLength: " + medium.length());
      for (int j = 0; j < medium.length(); j++) {
        // System.out.println(medium.getJSONObject(j));
        String id_to_medium = medium.getJSONObject(j).get("to_id").toString();
        // System.out.println(id_to_medium);
        String linkToMedium = readJsonObjectFromUrl(
            "https://classicmayan.kor.de.dariah.eu/entities/" + id_to_medium + ".json")
                .getJSONObject("medium").getJSONObject("url").get("original").toString();
        // String linkToMedium =
        // medium.getJSONObject(j).getJSONObject("to_id").getJSONObject("medium").getJSONObject("url").get("original").toString();
        String mediumID = readJsonObjectFromUrl(
            "https://classicmayan.kor.de.dariah.eu/entities/" + id_to_medium + ".json")
                .getJSONObject("medium").get("id").toString();
        // System.out.println(readJsonObjectFromUrl("https://classicmayan.kor.de.dariah.eu/entities/"
        // + id_to_medium + ".json"));
        // String mediumID =
        // medium.getJSONObject(j).getJSONObject("to_id").getJSONObject("medium").get("id").toString();
        // System.out.println("mediumID: " + mediumID);
        JSONObject mediaData = new JSONObject();
        mediaData.put("medium_id", mediumID);
        mediaData.put("link",
            linkToMedium.substring(0, linkToMedium.indexOf("?")).replace("jpg", "gif"));
        // System.out.println("DISPLAYNAME");
        // System.out.println(idsAndNamesOfArtefacts.getJSONObject(i).get("display_name"));

        // System.out.println(mediaData);
        // mediaArray.put(linkToMedium.substring(0, linkToMedium.indexOf("?")));

        mediaArray.put(mediaData);
        // System.out.println("FOREND "+ j);
      }
      // System.out.println(idsAndNamesOfArtefacts.getJSONObject(i).get("display_name"));
      singleResult.put("id", ids.get(i).toString());
      singleResult.put("display_name", idsAndNamesOfArtefacts.getJSONObject(i).get("display_name"));
      singleResult.putOnce("media", mediaArray);

      artefactIDsWithLinksToMediaArray.put(singleResult);
    }

    return new JSONObject().putOnce("result", artefactIDsWithLinksToMediaArray).toString(2);
  }

}
