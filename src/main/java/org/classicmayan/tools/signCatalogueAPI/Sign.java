package org.classicmayan.tools.signCatalogueAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.classicmayan.tools.IdiomConstants;
import org.classicmayan.tools.QueryUtillities;
import org.classicmayan.tools.TripleStoreQuery;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 */
@Path("/{textgridURI}")
public class Sign {

  JSONObject sign;
  JSONObject signParameters = new JSONObject();

  public String textgridURI;
  private List<String> fields = new ArrayList<String>();

  /**
   * 
   */
  private static final HashMap<String, String> signQueries = new HashMap<String, String>() {
    private static final long serialVersionUID = 1L;
    {
      put("status", "?textgridURI idiom:status ?status. \n");
      put("signNumber", "?textgridURI idiomcat:signNumber ?signNumber.\n");
      put("logographic_reading", IdiomConstants.SIGN_LOGOGRAPHIC_READING);
      put("logographic_meaning", IdiomConstants.SIGN_LOGOGRAPHIC_MEANING);
      put("syllabic_reading", IdiomConstants.SIGN_SYLLABIC_READING);
      put("diacritic_function", IdiomConstants.SIGN_DIACRITIC_FUNCTION);
      put("numeric_function", IdiomConstants.SIGN_NUMERIC_FUNCTION);
      put("distinct_allograph_to",
          "OPTIONAL{?textgridURI idiomcat:isDistinctAllographTo ?distinct_allograph_to.}\n");
      put("is_same_as_sign", "OPTIONAL{?textgridURI idiomcat:isSameAs ?is_same_as_sign.}");
      put("source", IdiomConstants.SOURCE);
    }
  };

  /**
   * 
   */
  public Sign() {
    this.sign = new JSONObject();
    // signParameters.put("identifier", this.getTextgridURI());
  }

  /**
   * @param textgridURI
   */
  public void setTextgridURI(@PathParam("textgridURI") String textgridURI) {
    this.textgridURI = textgridURI;
  }

  /**
   * @return
   */
  public String getTextgridURI() {
    return this.textgridURI;
  }

  /**
   * @param textgridURI
   * @return
   */
  @GET
  @Path("/signNumber")
  @Produces(MediaType.APPLICATION_JSON)
  public String getSignNumber(@PathParam("textgridURI") String textgridURI) {
    this.signParameters.put("identifier", textgridURI);
    this.signParameters.put("sign_number", TripleStoreQuery.getSignNumberForUri(textgridURI));
    this.sign.accumulate("sign", this.signParameters);
    return this.sign.toString(2);
  }

  /**
   * @param textgridURI
   * @return
   */
  @GET
  @Path("/logographicReading")
  public JSONObject getLogographicReading(@PathParam("textgridURI") String textgridURI) {

    // sign.accumulate(key, value)
    @SuppressWarnings("rawtypes")
    List<Pair> logographicReadingList =
        TripleStoreQuery.getLogographicReadingTransliterationValueWithLevel(textgridURI);
    for (Pair<?, ?> pair : logographicReadingList) {
      this.sign.append("logographic_reading", pair);
    }
    // sign.append("logographic_reading",
    // TripleStoreQuery.getLogographicReadingTransliterationValueWithLevel(textgridURI).toString());
    return this.sign;
    // return
    // TripleStoreQuery.getLogographicReadingTransliterationValueWithLevel(textgridURI).toString();
  }

  /**
   * @param textgridURI
   * @param fields
   * @return
   */
  @GET
  @Path("/fields={fieldlist}")
  @Produces(MediaType.APPLICATION_JSON)
  public String getSign(@PathParam("textgridURI") String textgridURI,
      @PathParam("fieldlist") String fields) {

    String query = fullFieldList(fields);

    return queryExecutionList(
        QueryUtillities.executeQuery(query.replace("URI_TO_REPLACE", textgridURI)), this.fields)
            .toString(2);
  }

  /**
   * @param fields
   * @return
   */
  public String fullFieldList(String fields) {

    if (this.fields.size() > 0) {
      this.fields.clear();
    }
    String query = "WHERE { GRAPH <URI_TO_REPLACE> {\n";
    String selects = "";
    for (String field : fields.split(",")) {
      query = query.concat(signQueries.get(field));
      selects = selects.concat(
          "(group_concat( distinct ?" + field + " ;separator=\"; \") as ?" + field + "s)\n");

      this.fields.add(field);
    }

    return IdiomConstants.SPARQL_PREFIXES + "\nSELECT " + selects + "\n\n" + query + " }}";
  }

  /**
   * @param fields
   * @param queries
   * @return
   */
  public String buildQuery(List<String> fields, HashMap<String, String> queries) {

    String query = "WHERE { GRAPH <URI_TO_REPLACE> {\n";
    String selects = "";
    for (String field : fields) {
      query = query.concat(queries.get(field));
      selects = selects.concat(
          "(group_concat( distinct ?" + field + " ;separator=\"; \") as ?" + field + "s)\n");
    }

    return IdiomConstants.SPARQL_PREFIXES + "\nSELECT " + selects + "\n\n" + query + " }}";
  }

  /**
   * @param results
   * @param fields
   * @return
   */
  public JSONObject queryExecutionList(ResultSet results, List<String> fields) {

    JSONObject jsonObject = new JSONObject();
    while (results.hasNext()) {
      QuerySolution processQuery = results.nextSolution();
      for (String field : fields) {
        if (processQuery.get(field + "s") != null) {
          String fieldContent = processQuery.get(field + "s").toString();

          if (field.contains("_reading") || field.contains("_meaning")
              || field.contains("_function")) {
            jsonObject.putOnce(field, getLinguisticAsJSON(field, fieldContent));
          } /*
             * else if((field.contains("_reading") || field.contains("_meaning") ||
             * field.contains("_function")) && !fieldContent.contains("; ")) {
             * jsonObject.putOnce(field, getTransliterationSingle(field, fieldContent)); }
             */
          else if (field.equals("source")) {
            jsonObject.putOnce(field, APIUtilities.getSourceAsJSONbla(fieldContent));
          }
          /*
           * else if(field.equals("source") && !fieldContent.contains("; ")) {
           * jsonObject.putOnce(field, getSingleSourceAsJSON(fieldContent)); }
           */
          else if (fieldContent.contains(";")) {
            jsonObject.putOnce(field, getValuesAsJSONArray(fieldContent));
          } else {
            jsonObject.accumulate(field, processQuery.get(field + "s").toString()
                .replace("http://textgridrep.de/", "textgrid:"));
          }
        }
      }
    }

    return jsonObject;
  }

  /**
   * @param fieldContent
   * @return
   */
  public JSONArray getValuesAsJSONArray(String fieldContent) {

    JSONArray fieldContentForJSONArray = new JSONArray();
    String[] fieldContentArray = fieldContent.split("; ");

    for (String content : fieldContentArray) {
      fieldContentForJSONArray.put(content.replace("http://textgridrep.de/", "textgrid:"));
    }

    return fieldContentForJSONArray;
  }

  /**
   * @param <T>
   * @param transliterationType
   * @param transliterationContent
   * @return
   */
  @SuppressWarnings("unchecked")
  public <T> T getLinguisticAsJSON(String transliterationType, String transliterationContent) {

    if (transliterationContent.contains("; ")) {
      return (T) getTransliteration(transliterationType, transliterationContent);
    } else {
      return (T) getTransliterationSingle(transliterationType, transliterationContent);
    }
  }

  /**
   * @param transliterationType
   * @param transliterationContent
   * @return
   */
  public static JSONArray getTransliteration(String transliterationType,
      String transliterationContent) {

    JSONArray transliterations = new JSONArray();
    String[] fieldConents = transliterationContent.split("; ");
    int i = 0;
    for (String fieldContent : fieldConents) {
      JSONObject subFields = new JSONObject();
      subFields.put("value", fieldContent.split(":")[0]);
      subFields.put("level", fieldContent.split(":")[1]);
      transliterations.put(i, subFields);
      i++;
    }

    return transliterations;
  }

  /**
   * @param transliterationType
   * @param transliterationContent
   * @return
   */
  public static JSONObject getTransliterationSingle(String transliterationType,
      String transliterationContent) {

    JSONObject subFields = new JSONObject();

    subFields.put("value", transliterationContent.split(":")[0]);
    subFields.put("level", transliterationContent.split(":")[1]);

    return subFields;
  }

  /**
   * @param query
   * @return
   */
  public static ResultSet executeQuery(String query) {

    QueryExecution queryToExecute =
        QueryExecutionFactory.sparqlService(IdiomConstants.SPARQL_METADATA_ENDPOINT, query);
    ResultSet results = queryToExecute.execSelect();

    return results;
  }

  /**
   * @return
   */
  public HashMap<String, String> getSignQueries() {
    return signQueries;
  }

}
