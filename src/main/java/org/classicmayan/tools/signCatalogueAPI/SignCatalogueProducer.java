package org.classicmayan.tools.signCatalogueAPI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.classicmayan.tools.signCatalogueAPI.SignCatalogueProducer;

/**
 * FIXME Can we delete this class?
 */
@Path("/")
public class SignCatalogueProducer implements SignCatalogueAPI {

  private Log log = LogFactory.getLog(SignCatalogueProducer.class);

  /**
   * @return
   */
  @GET
  @Path("/")
  public String hello() {
    return "hello";
  }

}
