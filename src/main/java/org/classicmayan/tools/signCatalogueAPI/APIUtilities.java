package org.classicmayan.tools.signCatalogueAPI;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 */
public class APIUtilities {

  /**
   * @param <T>
   * @param source
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> T getSourceAsJSONbla(String source) {

    if (source.contains("; ")) {
      return (T) getSourceAsJSONArray(source);
    } else {
      return (T) getSingleSourceAsJSON(source);
    }
  }

  /**
   * @param sources
   * @return
   */
  public static JSONArray getSourceAsJSONArray(String sources) {

    JSONArray transliterations = new JSONArray();
    String[] singleSources = sources.split("; ");
    int i = 0;

    for (String source : singleSources) {
      JSONObject sourceFields = new JSONObject();

      sourceFields.put("zoteroID", source.split("%")[0]);
      sourceFields.put("bibliographicCitation", source.split("%")[1]);
      sourceFields.put("page", source.split("%")[2]);
      sourceFields.put("attributionType", source.split("%")[3]);
      sourceFields.put("confidenceLevel", source.split("%")[4]);

      transliterations.put(i, sourceFields);
      i++;
    }

    return transliterations;
  }

  /**
   * @param sources
   * @return
   */
  public static JSONObject getSingleSourceAsJSON(String sources) {

    JSONObject sourceFields = new JSONObject();

    sourceFields.put("zoteroID", sources.split("%")[0]);
    sourceFields.put("bibliographicCitation", sources.split("%")[1]);
    sourceFields.put("page", sources.split("%")[2]);
    sourceFields.put("attributionType", sources.split("%")[3]);
    sourceFields.put("confidenceLevel", sources.split("%")[4]);

    return sourceFields;
  }

}
